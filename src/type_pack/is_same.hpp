#ifndef TYPE_PACK_IS_SAME_HPP
#define TYPE_PACK_IS_SAME_HPP

namespace tp {

template <typename T, typename U>
struct is_same {
    static constexpr bool value = false;
};

template <typename T>
struct is_same<T, T> {
    static constexpr bool value = true;
};

template <typename T, typename U>
constexpr bool is_same_v = is_same<T, U>::value;

}  // namespace tp

#endif  // TYPE_PACK_IS_SAME_HPP