#ifndef TYPE_PACK_SINGLE_TYPE_HPP
#define TYPE_PACK_SINGLE_TYPE_HPP

namespace tp {

template <typename T>
struct single_type {
    using type = T;
};

}  // namespace tp

#endif  // TYPE_PACK_SINGLE_TYPE_HPP:w