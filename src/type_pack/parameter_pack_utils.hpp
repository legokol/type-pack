#ifndef TYPE_PACK_PARAMETER_PACK_UTILS_HPP
#define TYPE_PACK_PARAMETER_PACK_UTILS_HPP

#include <utility>

#include "single_type.hpp"

namespace tp {

namespace details {

template <std::size_t I, typename T>
struct indexed_type {
    static constexpr std::size_t value = I;
    using type                         = T;
};

template <typename, typename...>
struct indexed_types;

template <std::size_t... Is, typename... Ts>
struct indexed_types<std::index_sequence<Is...>, Ts...> {
    struct type : indexed_type<Is, Ts>... {};
};

template <typename... Ts>
using indexed_types_for = typename indexed_types<std::index_sequence_for<Ts...>, Ts...>::type;

template <std::size_t I, typename T>
constexpr single_type<T> get_indexed_type(indexed_type<I, T>) noexcept {
    return {};
}

template <typename T, typename... Ts>
constexpr bool same_types_impl() noexcept {
    return std::conjunction_v<std::is_same<T, Ts>...>;
}

}  // namespace details

/**
 * Checks whether parameter pack contains a specific type
 * @tparam T type to search for
 * @tparam Ts parameter pack
 * @return true if Ts contains T, false otherwise
 */
template <typename T, typename... Ts>
constexpr bool contains() noexcept {
    return std::disjunction_v<std::is_same<T, Ts>...>;
}

/**
 * Finds index of a specific type in a parameter pack
 * @tparam T type to search for
 * @tparam Ts parameter pack
 * @return index of the first element of type T in Ts if it exists, size of Ts otherwise
 */
template <typename T, typename... Ts>
constexpr std::size_t find() noexcept {
    if constexpr (sizeof...(Ts) == 0) {
        return false;
    } else {
        constexpr bool is_true[] = {std::is_same_v<T, Ts>...};
        for (std::size_t i = 0; i < sizeof...(Ts); ++i) {
            if (is_true[i]) {
                return i;
            }
        }
        return sizeof...(Ts);
    }
}

/**
 * Finds index of the first type in a parameter pack satisfying specific criteria
 * @tparam F predicate which returns true for the required element
 * @tparam Ts parameter pack
 * @return index of the first element satisfying F if it exists, size of Ts otherwise
 */
template <template <typename...> typename F, typename... Ts>
constexpr std::size_t find_if() noexcept {
    constexpr bool is_true[] = {F<Ts>::value...};
    for (std::size_t i = 0; i < sizeof...(Ts); ++i) {
        if (is_true[i]) {
            return i;
        }
    }
    return sizeof...(Ts);
}

/**
 * Returns the number of types in a parameter pack same as a specific type
 * @tparam T type to search for
 * @tparam Ts parameter pack
 * @return number of types in Ts same as T
 */
template <typename T, typename... Ts>
constexpr std::size_t count() noexcept {
    if constexpr (sizeof...(Ts) == 0) {
        return false;
    } else {
        constexpr bool is_true[] = {std::is_same_v<T, Ts>...};
        std::size_t c            = 0;
        for (std::size_t i = 0; i < sizeof...(Ts); ++i) {
            if (is_true[i]) {
                ++c;
            }
        }
        return c;
    }
}

/**
 * Returns the number of types in a parameter pack satisfying specific criteria
 * @tparam F predicate which returns true for the required element
 * @tparam Ts parameter pack
 * @return number of types in Ts satisfying F
 */
template <template <typename...> typename F, typename... Ts>
constexpr std::size_t count_if() noexcept {
    constexpr bool is_true[] = {F<Ts>::value...};
    std::size_t c            = 0;
    for (std::size_t i = 0; i < sizeof...(Ts); ++i) {
        if (is_true[i]) {
            ++c;
        }
    }
    return c;
}

/**
 * Checks whether parameter pack consists of only one type
 * @tparam Ts parameter pack
 * @return true if Ts consists of one type, false otherwise
 */
template <typename... Ts>
constexpr bool same_types() noexcept {
    if constexpr (sizeof...(Ts) == 0) {
        return true;
    } else {
        return details::same_types_impl<Ts...>();
    }
}

/**
 * Checks whether parameter pack contains unique elements (exactly one entry of each type)
 * @tparam Ts parameter pack
 * @return true if Ts is unique, false otherwise
 */
template <typename... Ts>
constexpr bool unique() noexcept {
    return ((count<Ts, Ts...>() == 1) && ...);
}

/**
 * Gets type from parameter pack using index
 * @tparam I index of the required type
 * @tparam Ts parameter pack
 * @return struct single_type, containing required type T
 */
template <std::size_t I, typename... Ts>
constexpr auto get() noexcept {
    return details::get_indexed_type<I>(details::indexed_types_for<Ts...>{});
}

template <std::size_t I, typename... Ts>
using get_t = typename decltype(get<I, Ts...>())::type;

}  // namespace tp

#endif  // TYPE_PACK_PARAMETER_PACK_UTILS_HPP