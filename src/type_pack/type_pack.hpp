#ifndef TYPE_PACK_TYPE_PACK_HPP
#define TYPE_PACK_TYPE_PACK_HPP

#include "is_same.hpp"
#include "parameter_pack_utils.hpp"
#include "single_type.hpp"

namespace tp {

/**
 * Struct to store parameter packs
 * @tparam Ts parameter pack
 */
template <typename... Ts>
struct type_pack {
    /**
     * Returns size of parameter pack
     * @return sizeof...(Ts)
     */
    [[nodiscard]] constexpr std::size_t size() const { return sizeof...(Ts); }
};

/**
 * Compares two type_packs with different parameter packs
 * @tparam Ts parameter pack, stored in the first type_pack
 * @tparam Us parameter pack, stored in the second type_pack (different from Ts)
 * @return false
 */
template <typename... Ts, typename... Us>
constexpr bool operator==(type_pack<Ts...>, type_pack<Us...>) noexcept {
    return false;
}

/**
 * Compares two type_packs with same parameter packs
 * @tparam Ts parameter pack
 * @return true
 */
template <typename... Ts>
constexpr bool operator==(type_pack<Ts...>, type_pack<Ts...>) noexcept {
    return true;
}

/**
 * Concatenates two type_packs
 * @tparam Ts parameter pack, stored in the first type_pack
 * @tparam Us parameter pack, stored in the second type_pack
 * @return concatenated type_pack
 */
template <typename... Ts, typename... Us>
constexpr type_pack<Ts..., Us...> operator+(type_pack<Ts...>, type_pack<Us...>) noexcept {
    return {};
}

/**
 * Adds type T to type_pack, unless it is already present
 * @tparam Ts stored parameter pack
 * @tparam T new type
 * @return type_pack with added type, if it's new, or the same type_pack
 */
template <typename... Ts, typename T>
constexpr auto operator|(type_pack<Ts...>, type_pack<T>) noexcept {
    if constexpr (contains<T, Ts...>()) {
        return type_pack<Ts...>{};
    } else {
        return type_pack<Ts..., T>{};
    }
}

/**
 * Adds types Us to type_pack, unless they are already present. Adds each type once.
 * @tparam Us new parameter pack
 * @return type_pack with added types, if they're new
 */
template <typename... Ts, typename... Us>
constexpr auto operator|(type_pack<Ts...>, type_pack<Us...>) noexcept {
    return (type_pack<Ts...>{} | ... | type_pack<Us>{});
}

using empty_pack = type_pack<>;

/**
 * Returns size of parameter pack, stored in type_pack
 * @tparam Ts parameter pack
 * @return sizeof...(Ts)
 */
template <typename... Ts>
constexpr unsigned long long size(type_pack<Ts...>) noexcept {
    return sizeof...(Ts);
}

/**
 * Checks whether type_pack contains a specific type
 * @tparam T type to search for
 * @tparam Ts parameter pack
 * @return true if Ts contains T, false otherwise
 */
template <typename T, typename... Ts>
constexpr bool contains(type_pack<Ts...>) noexcept {
    return contains<T, Ts...>();
}

/**
 * Finds index of a specific type in a type_pack
 * @tparam T type to search for
 * @tparam Ts parameter pack
 * @return index of the first element of type T in Ts if it exists, size of Ts otherwise
 */
template <typename T, typename... Ts>
constexpr std::size_t find(type_pack<Ts...>) noexcept {
    return find<T, Ts...>();
}

/**
 * Finds index of the first type in a type_pack satisfying specific criteria
 * @tparam F predicate which returns true for the required element
 * @tparam Ts parameter pack
 * @return index of the first element satisfying F if it exists, size of Ts otherwise
 */
template <template <typename...> typename F, typename... Ts>
constexpr std::size_t find_if(type_pack<Ts...>) noexcept {
    return find_if<F, Ts...>();
}

/**
 * Returns the number of types in a type_pack same as a specific type
 * @tparam T type to search for
 * @tparam Ts parameter pack
 * @return number of types in Ts same as T
 */
template <typename T, typename... Ts>
constexpr std::size_t count(type_pack<Ts...>) noexcept {
    return count<T, Ts...>();
}

/**
 * Returns the number of types in a type_pack satisfying specific criteria
 * @tparam F predicate which returns true for the required element
 * @tparam Ts parameter pack
 * @return number of types in Ts satisfying F
 */
template <template <typename...> typename F, typename... Ts>
constexpr std::size_t count_if(type_pack<Ts...>) noexcept {
    return count_if<F, Ts...>();
}

/**
 * Checks whether type_pack contains elements of the same type
 * @tparam Ts parameter pack
 * @return true if Ts consists of one type, false otherwise
 */
template <typename... Ts>
constexpr bool same_types(type_pack<Ts...>) noexcept {
    return same_types<Ts...>();
}

/**
 * Checks whether type_pack contains unique elements (exactly one entry of each type)
 * @tparam Ts parameter pack
 * @return true if Ts is unique, false otherwise
 */
template <typename... Ts>
constexpr bool unique(type_pack<Ts...>) noexcept {
    return unique<Ts...>();
}

/**
 * Gets type from type_pack using index
 * @tparam I index of the required type
 * @tparam Ts parameter pack
 * @return struct single_type, containing required type T
 */
template <std::size_t I, typename... Ts>
constexpr auto get(type_pack<Ts...>) noexcept {
    return get<I, Ts...>();
}

/**
 * Gets unique elements from type_pack
 * @tparam Ts parameter pack
 * @return type_pack, containing exactly one copy of each type from Ts
 */
template <typename... Ts>
constexpr auto make_unique(type_pack<Ts...>) noexcept {
    return ((empty_pack{} | ... | type_pack<Ts>{}));
}

template <typename TypePack>
using make_unique_t = decltype(make_unique(TypePack{}));

namespace details {

template <template <typename...> typename F, typename T>
constexpr auto filter_single() noexcept {
    if constexpr (F<T>::value) {
        return type_pack<T>{};
    } else {
        return empty_pack{};
    }
}

}  // namespace details

/**
 * Filters type_pack
 * @tparam F predicate, which returns true for required elements
 * @tparam Ts parameter pack
 * @return type_pack containing types from Ts satisfying F
 */
template <template <typename...> typename F, typename... Ts>
constexpr auto filter(type_pack<Ts...>) noexcept {
    return ((empty_pack{} + ... + details::filter_single<F, Ts>()));
}

template <template <typename...> typename F, typename TypePack>
using filter_t = decltype(filter<F>(TypePack{}));

/**
 * Transforms type_pack
 * @tparam F function to apply
 * @tparam Ts parameter pack
 * @return type_pack, containing F<Ts>
 */
template <template <typename...> typename F, typename... Ts>
constexpr type_pack<F<Ts>...> transform(type_pack<Ts...>) noexcept {
    return {};
}

/**
 * Substitutes parameter pack, contained within type_pack, into a specific type
 * @tparam F template to substitute to
 * @tparam Ts parameter pack
 * @return single_type, containing F<Ts...>
 */
template <template <typename...> typename F, typename... Ts>
constexpr single_type<F<Ts...>> substitute(type_pack<Ts...>) noexcept {
    return {};
}

}  // namespace tp

#endif  // TYPE_PACK_TYPE_PACK_HPP
