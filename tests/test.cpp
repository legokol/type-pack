#include <gtest/gtest.h>

#include "type_pack/type_pack.hpp"

TEST(parameter_pack_utils, contains) {
    EXPECT_TRUE((tp::contains<int, int, double>()));
    EXPECT_TRUE((tp::contains<int, double, int>()));
    EXPECT_TRUE((!tp::contains<int>()));
    EXPECT_TRUE((!tp::contains<int, double>()));
}

TEST(parameter_pack_utils, find) {
    EXPECT_EQ(tp::find<int>(), 0);
    EXPECT_EQ((tp::find<int, int, double>()), 0);
    EXPECT_EQ((tp::find<int, double, int>()), 1);
    EXPECT_EQ((tp::find<int, int, int>()), 0);
    EXPECT_EQ((tp::find<int, double>()), 1);

    EXPECT_EQ((tp::find_if<std::is_void, int, double>()), 2);
    EXPECT_EQ((tp::find_if<std::is_void, double, void>()), 1);
}

TEST(parameter_pack_utils, count) {
    EXPECT_EQ(tp::count<int>(), 0);
    EXPECT_EQ((tp::count<int, int, double>()), 1);
    EXPECT_EQ((tp::count<int, double, int>()), 1);
    EXPECT_EQ((tp::count<int, int, int>()), 2);

    EXPECT_EQ((tp::count_if<std::is_void, int, double>()), 0);
    EXPECT_EQ((tp::count_if<std::is_void, double, void>()), 1);
}

TEST(parameter_pack_utils, same_types) {
    EXPECT_TRUE((tp::same_types<>()));
    EXPECT_TRUE((tp::same_types<int>()));
    EXPECT_TRUE((tp::same_types<int, int>()));
    EXPECT_TRUE((!tp::same_types<int, double>()));
}

TEST(parameter_pack_utils, unique) {
    EXPECT_TRUE((tp::unique<>()));
    EXPECT_TRUE((tp::unique<int, double>()));
    EXPECT_TRUE((!tp::unique<int, int>()));
}

TEST(parameter_pack_utils, get) {
    EXPECT_TRUE((std::is_same_v<int, tp::get_t<0, int>>));
    EXPECT_TRUE((std::is_same_v<int, tp::get_t<1, double, int>>));
    EXPECT_TRUE((std::is_same_v<int&, tp::get_t<1, double&, int&>>));
    EXPECT_TRUE((std::is_same_v<const double&, tp::get_t<0, const double&, int&>>));
}

TEST(type_pack, operators) {
    EXPECT_TRUE(tp::type_pack<>{} == tp::type_pack<>{});
    EXPECT_TRUE(tp::type_pack<int>{} == tp::type_pack<int>{});
    EXPECT_FALSE(tp::type_pack<>{} == tp::type_pack<int>{});
    EXPECT_FALSE((tp::type_pack<int>{} == tp::type_pack<int, double>{}));

    EXPECT_TRUE(tp::type_pack<int>{} == tp::type_pack<>{} + tp::type_pack<int>{});
    EXPECT_TRUE((tp::type_pack<int, int>{} == tp::type_pack<int>{} + tp::type_pack<int>{}));

    EXPECT_TRUE((tp::type_pack<int>{} == (tp::type_pack<int>{} | tp::type_pack<int>{})));
    EXPECT_TRUE((tp::type_pack<int, double>{} == (tp::type_pack<int>{} | tp::type_pack<int, double, double>{})));
}

TEST(type_pack, unique) {
    EXPECT_TRUE((tp::type_pack<int, double>{} == tp::make_unique(tp::type_pack<int, double, double, int>{})));
}

TEST(type_pack, filter) {
    EXPECT_TRUE(
        (tp::type_pack<double*, int*>{} == tp::filter<std::is_pointer>(tp::type_pack<int, double, double*, int*>{})));
}